﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Expirience : MonoBehaviour {

    
    public Slider expirienceSlider;
    public Text levelText;
    AttributesFoAppLvl AppLvlScript;
    

    void Start() {
        AppLvlScript = GameObject.Find("PlayerStatsVaribels").GetComponent<AttributesFoAppLvl>();
        AppLvlScript.levelUpCount = 1;
        levelText.text = AppLvlScript.level.ToString();
        expirienceSlider.maxValue = 200 + AppLvlScript.level * 100;
    }
    
    public void PlusExpirience() {
        
        if (AppLvlScript.level == AppLvlScript.maxLevel) { expirienceSlider.value = expirienceSlider.maxValue; }
        else
        {
            if (AppLvlScript.level >= 1)
            {
                if (AppLvlScript.expirience + expirienceSlider.value >= expirienceSlider.maxValue)
                {
                    if (AppLvlScript.levelUpCount > 1)
                    {
                        AppLvlScript.level += AppLvlScript.levelUpCount;
                        expirienceSlider.maxValue += AppLvlScript.levelUpCount * 100;
                        AppLvlScript.levelUpCount = 1;
                        AppLvlScript.countSkills += 4 * AppLvlScript.levelUpCount;
                    }
                    else
                    {
                        AppLvlScript.level += 1;
                        expirienceSlider.maxValue += 100;
                        AppLvlScript.countSkills += 4;
                    }
                    levelText.text = AppLvlScript.level.ToString();
                    expirienceSlider.value = 0;
                }
                else
                {
                    expirienceSlider.value += AppLvlScript.expirience;
                }
            }
        }
    }


    public void PlusStrenght()
    {
        AppLvlScript.expirience = 100;
        PlusExpirience();
    }
}
