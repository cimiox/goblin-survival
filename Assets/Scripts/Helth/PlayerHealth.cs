﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerHealth : MonoBehaviour
{
    
     //The current health the player has.
    public Slider healthSlider;
    public Slider manaSlider;// Reference to the UI's health bar.
    public Image damageImage;                                   // Reference to an image to flash on the screen on being hurt.
    public AudioClip deathClip;                                 // The audio clip to play when the player dies.
    public float flashSpeed = 5f;                               // The speed the damageImage will fade at.
    public Color flashColour = new Color(1f, 0f, 0f, 0.1f);     // The colour the damageImage is set to, to flash.
    public Text textLevel;
    
    Expirience expirienceScript;
    PlayerStats attributesForPlayer;
    Animator anim;                                              // Reference to the Animator component.
    AudioSource playerAudio;                                    // Reference to the AudioSource component.
    GoMobile playerMovement;                              // Reference to the player's movement.
    PlayerShooting playerShooting;                              

    bool isDead;                                                // Whether the player is dead.
    bool damaged;
    bool spendMana;// True when the player gets damaged.
    



    void Start()
    {
        
        attributesForPlayer = GameObject.Find("PlayerStatsVaribels").GetComponent<PlayerStats>();
        //получение различных компонентов
        anim = GetComponent<Animator>();
        playerAudio = GetComponent<AudioSource>();
        playerMovement = GetComponent<GoMobile>();
        playerShooting = GetComponentInChildren<PlayerShooting>();
        
        //подсчёт атаки
        playerShooting.damagePerShot = attributesForPlayer.strenght;
               
        //подсчёт защиты
        attributesForPlayer.defense = attributesForPlayer.agility / 7;       
        
        //Получаем стартовое хп и ману
        attributesForPlayer.currentHealth = attributesForPlayer.startingHealth + (attributesForPlayer.strenght * 20);
        attributesForPlayer.currentMana = attributesForPlayer.startingMana + (attributesForPlayer.intellect * 15);
        
        //ставим на слайдер полученные значения
        healthSlider.maxValue = attributesForPlayer.currentHealth;
        healthSlider.value = attributesForPlayer.currentHealth;
        manaSlider.maxValue = attributesForPlayer.currentMana;
        manaSlider.value = attributesForPlayer.currentMana;
        
    }


    void Update()
    {    
        //картинка получения урона
        if (damaged)
        {
            damageImage.color = flashColour;
        }
        else
        {
            damageImage.color = Color.Lerp(damageImage.color, Color.clear, flashSpeed * Time.deltaTime);
        }      
        damaged = false;
        
        //Регенерация хп и маны
        if (attributesForPlayer.currentHealth < healthSlider.maxValue)
        {
            attributesForPlayer.regenerationHP = attributesForPlayer.strenght * 0.7f;
            attributesForPlayer.currentHealth += attributesForPlayer.regenerationHP * Time.deltaTime;
            healthSlider.value = attributesForPlayer.currentHealth;
        }
        if (attributesForPlayer.currentMana < manaSlider.maxValue)
        {
            attributesForPlayer.regenerationMP = attributesForPlayer.intellect * 0.8f;
            attributesForPlayer.currentMana += attributesForPlayer.regenerationMP * Time.deltaTime;
            healthSlider.value = attributesForPlayer.currentMana;
        }
    }


    //
    public void TakeDamage(float amount)
    {
        // Set the damaged flag so the screen will flash.
        damaged = true;

        attributesForPlayer.currentHealth -= amount;
        // Set the health bar's value to the current health.
        healthSlider.value = attributesForPlayer.currentHealth;

        // Play the hurt sound effect.
        playerAudio.Play();

        // If the player has lost all it's health and the death flag hasn't been set yet...
        if (attributesForPlayer.currentHealth <= 0 && !isDead)
        {
            // ... it should die.
            Death();
        }
    }

    void ManaPoint(int amount){
        spendMana = true;

        attributesForPlayer.currentMana -= amount;

        manaSlider.value = attributesForPlayer.currentMana;

        if (attributesForPlayer.currentMana <= 0)
            playerAudio.Play();
    }

    void Death()
    {
        // Set the death flag so this function won't be called again.
        isDead = true;

        // Turn off any remaining shooting effects.
        //playerShooting.DisableEffects();

        // Tell the animator that the player is dead.
        anim.SetTrigger("Die");

        // Set the audiosource to play the death clip and play it (this will stop the hurt sound from playing).
        playerAudio.clip = deathClip;

        playerAudio.Play();

        // Turn off the movement and shooting scripts.
        playerMovement.enabled = false;
        //playerShooting.enabled = false;
    }

    
}