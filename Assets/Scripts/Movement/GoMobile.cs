﻿using System;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

 
        
public class GoMobile : MonoBehaviour
{
        public float speed;
        private Rigidbody rb;
        Vector3 movement;       

        Animator anim;
        int floorMask;
        float camRayLength = 100f;

    PlayerShooting ShootingScript;
 
    /*void Awake()
        {
        floorMask = LayerMask.GetMask("Floor");
        anim = GetComponent<Animator>();

        }*/

		
    private void Start()
    {
        ShootingScript = GameObject.Find("GunParticles").GetComponent<PlayerShooting>();
        rb = GetComponent<Rigidbody>();
    }


    
    private void FixedUpdate() {
        float h = CrossPlatformInputManager.GetAxis("Horizontal");
        float v = CrossPlatformInputManager.GetAxis("Vertical");            

        movement.Set(h, 0.0f, v);
           
        movement = movement.normalized * speed * Time.deltaTime;
        rb.MovePosition(transform.position + movement );

        if (ShootingScript.isPressed) {
            StopRotation();
            Animating(h, v);
        }
        else {
            Rotation(h, v);
            Animating(h, v);
        }
            
    }


    void Rotation(float h, float v) {
            if (h != 0 && v != 0)
            {
                h= h / 2;
                v= v / 2;

                Vector3 playerRotate = new Vector3(h, 0, v);
                playerRotate.y = 0f;
                Quaternion newRotation = Quaternion.LookRotation(playerRotate);
                rb.MoveRotation(newRotation);
            }
             
    }

    void Turning()
    {
            Ray camRay = Camera.current.ScreenPointToRay(Input.mousePosition);

            RaycastHit floorHit;

            if (Physics.Raycast(camRay, out floorHit, camRayLength, floorMask))
            {
                Vector3 playerToMouse = floorHit.point - transform.position;
                playerToMouse.y = 0f;

                Quaternion newRotation = Quaternion.LookRotation(playerToMouse);
                rb.MoveRotation(newRotation);
            }
    }

    void Animating(float h, float v)
    {
            bool walking = h != 0f || v != 0f;
            //anim.SetBool("Walk", walking);
			
			 
			
            /*bool die = d != 0f;
			 
			if(Die==true)
			{
				anim.SetTrigger("PlayerDead");
			}*/  
    }

    void StopRotation() {
		transform.Translate(new Vector3(0,0,0));
    }
		 
}
 







