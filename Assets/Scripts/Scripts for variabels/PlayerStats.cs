﻿using UnityEngine;
using System.Collections;

public class PlayerStats : MonoBehaviour {
    private static bool created;
    void OnEnable()
    {
        if (created)
        {
            Destroy(gameObject);
        }
        else
        {
            created = true;
            DontDestroyOnLoad(gameObject);
        }
    }

    public float startingHealth = 100;
    public float startingMana = 100;
    public float strenght = 15;
    public float regenerationHP = 1f;
    public float agility;
    public float defense;
    public float intellect;
    public float regenerationMP;
    public float currentHealth;
    public float currentMana;


}
