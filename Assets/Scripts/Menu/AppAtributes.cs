﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class AppAtributes : MonoBehaviour {

    
    public Button plusStrenght;
    public Button plusAgility;
    public Button plusIntellect;
    public Text skillsCountText;
    public Text strenghtCount;
    public Text agilityCount;
    public Text intellectCount;

    AttributesFoAppLvl AppLvlScript;
    PlayerStats attributesForPlayer;

	void Start () {
        attributesForPlayer = GameObject.Find("PlayerStatsVaribels").GetComponent<PlayerStats>();
        AppLvlScript = GameObject.Find("PlayerStatsVaribels").GetComponent<AttributesFoAppLvl>();
        agilityCount.text = attributesForPlayer.agility.ToString();
		strenghtCount.text = attributesForPlayer.strenght.ToString ();
        intellectCount.text = attributesForPlayer.intellect.ToString();
        skillsCountText.text = AppLvlScript.countSkills.ToString();
        SkillsCount();
	}

    public void SkillsCount() {
        if (AppLvlScript.countSkills > 0)
        {
            plusStrenght.gameObject.SetActive(true);
            plusAgility.gameObject.SetActive(true);
            plusIntellect.gameObject.SetActive(true);
            
        }
        else {
            plusStrenght.gameObject.SetActive(false);
            plusAgility.gameObject.SetActive(false);
            plusIntellect.gameObject.SetActive(false);
        }
    
    }

    public void PlusStrenght() {
        if (AppLvlScript.countSkills != 0)
        {
            attributesForPlayer.strenght++;
            AppLvlScript.countSkills--;
            strenghtCount.text = attributesForPlayer.strenght.ToString();
            skillsCountText.text = AppLvlScript.countSkills.ToString();
        }
        else { SkillsCount(); }
    }
    public void PlusAgility()
    {
        if (AppLvlScript.countSkills != 0)
        {
            attributesForPlayer.agility++;
            AppLvlScript.countSkills--;
            strenghtCount.text = attributesForPlayer.strenght.ToString();
            skillsCountText.text = AppLvlScript.countSkills.ToString();
        }
        else { SkillsCount(); }
    }
    public void PlusIntellect()
    {
        if (AppLvlScript.countSkills != 0)
        {
            attributesForPlayer.intellect++;
            AppLvlScript.countSkills--;
            strenghtCount.text = attributesForPlayer.strenght.ToString();
            skillsCountText.text = AppLvlScript.countSkills.ToString();
        }
        else { SkillsCount(); }
    }
}
